package com.cambodia.order.services.utils;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class Utils {
    public static final int CREATED_CODE = 200;
    public static final int BAD_REQUEST = 400;

    public static final String MESSAGE_COMPLETE = "success";
}
