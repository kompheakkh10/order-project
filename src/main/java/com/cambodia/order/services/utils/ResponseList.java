package com.cambodia.order.services.utils;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ResponseList<T>{

    private int status;
    private String message;
    private List<T> data;

}
