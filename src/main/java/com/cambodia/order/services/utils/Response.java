package com.cambodia.order.services.utils;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class Response<T>{

    private int status;
    private String message;
    private T data;

}
