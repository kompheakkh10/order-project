package com.cambodia.order.services.service;

import com.cambodia.order.services.dto.OrderLineItemDto;
import com.cambodia.order.services.dto.OrderRequest;
import com.cambodia.order.services.entity.Order;
import com.cambodia.order.services.entity.OrderLineItems;
import com.cambodia.order.services.repository.OrderRepository;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Transactional
public class OrderService {

    private OrderRepository orderRepository;

    private final WebClient webClient;

    public void placeOrder(OrderRequest orderRequest){
        Order order = new Order();
        order.setOrderNumber(UUID.randomUUID().toString());

        List<OrderLineItems> orderLineItems = orderRequest.getOrderLineItemsDto()
                .stream()
                .map(this::mapToEntity)
                .collect(Collectors.toList());

        order.setOrderLineItems(orderLineItems);

        // call inventory service
        /*webClient.get()*/
        orderRepository.save(order);
    }
    private OrderLineItems mapToEntity(OrderLineItemDto orderLineItemDto){
        OrderLineItems orderLineItem = new OrderLineItems();
        orderLineItem.setId(orderLineItemDto.getId());
        orderLineItem.setPrice(orderLineItemDto.getPrice());
        orderLineItem.setSkuCode(orderLineItemDto.getSkuCode());
        orderLineItem.setQuantity(orderLineItemDto.getQuantity());

        return orderLineItem;
    }

}
