package com.cambodia.order.services.controller;

import com.cambodia.order.services.dto.ApiResponse;
import com.cambodia.order.services.dto.OrderRequest;
import com.cambodia.order.services.service.OrderService;
import com.cambodia.order.services.utils.OrderMessage;
import com.cambodia.order.services.utils.Utils;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/order")
@AllArgsConstructor
public class OrderController {
    private final OrderService orderService;

    @PostMapping("/place_order")
    public ResponseEntity<ApiResponse<?>> placeOrder(@RequestBody OrderRequest orderRequest){
        orderService.placeOrder(orderRequest);
        ApiResponse<String> apiResponse = new ApiResponse<>(
                Utils.CREATED_CODE,
                Utils.MESSAGE_COMPLETE,
                OrderMessage.ORDER_SUCCESS
        );

        return ResponseEntity.ok(apiResponse);
    }
}
