package com.cambodia.order.services.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ApiResponseDetails<T>{
    private int status;
    private String message;
    private List<T> data;
}
